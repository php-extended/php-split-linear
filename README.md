# php-extended/php-split-linear
A library that implements php-split-interface with linear splitters

![coverage](https://gitlab.com/php-extended/php-split-linear/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-split-linear/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-split-linear ^8`


## Basic Usage

// TODO


## License

MIT (See [license file](LICENSE)).
