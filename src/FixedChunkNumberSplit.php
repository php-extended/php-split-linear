<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-split-linear library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Split;

use RuntimeException;

/**
 * FixedChunkNumberSplit class file.
 * 
 * This class splits a file into a specified number of chunks with equal size.
 * 
 * @author Anastaszor
 */
class FixedChunkNumberSplit extends AbstractSplit
{
	
	/**
	 * The number of chunks to split the file into.
	 * 
	 * @var integer
	 */
	protected int $_nbChunks = 1;
	
	/**
	 * Builds a new FixedChunkNumberSplit with the given chunk quantity. The
	 * quantity cannot be lower than one.
	 * 
	 * @param integer $nbChunks
	 */
	public function __construct(int $nbChunks = 1)
	{
		if(0 > $nbChunks)
		{
			$this->_nbChunks = $nbChunks;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Split\SplitInterface::fileSplit()
	 */
	public function fileSplit(string $sourceFileName, string $destinationDirectory) : array
	{
		$realpath = \realpath($sourceFileName);
		if(false === $realpath)
		{
			throw new RuntimeException('Failed to find realpath for file at '.$sourceFileName);
		}
		$source = $this->getSourceFileResource($realpath);
		$size = \filesize($realpath);
		if(false === $size)
		{
			$this->releaseFileResource($source);
			
			throw new RuntimeException('Failed to get filesize for file at '.$realpath);
		}
		
		$chunksize = (int) \ceil((((float) $size) / ((float) $this->_nbChunks)));
		$nbplen = \max(3, (int) \mb_strlen("{$this->_nbChunks}"));
		$destinationRealPaths = [];
		
		for($i = 0; $i < $this->_nbChunks; $i++)
		{
			$offset = $i * $chunksize;
			$newFileName = \basename($sourceFileName).'.'.\str_pad("{$i}", $nbplen, '0', \STR_PAD_LEFT);
			$destinationPath = $destinationDirectory.'/'.$newFileName;
			$destinationRealPaths[] = $destinationPath;
			$destination = $this->getDestinationFileResource($destinationPath);
			$res = \stream_copy_to_stream($source, $destination, $chunksize, $offset);
			$this->releaseFileResource($destination);
			if(false === $res)
			{
				$this->releaseFileResource($source);
				
				throw new RuntimeException('Failed to copy data from '.$sourceFileName.' to '.$destinationPath);
			}
		}
		$this->releaseFileResource($source);
		
		return $destinationRealPaths;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Split\SplitInterface::fileJoin()
	 */
	public function fileJoin(array $sourceFileNames, string $destinationFile) : string
	{
		$realpath = \realpath($destinationFile);
		if(false === $realpath)
		{
			throw new RuntimeException('Failed to find realpath for file at '.$destinationFile);
		}
		
		$destination = $this->getDestinationFileResource($realpath);
		
		foreach($sourceFileNames as $sourceFileName)
		{
			$source = $this->getSourceFileResource($sourceFileName);
			$res = \stream_copy_to_stream($source, $destination);
			$this->releaseFileResource($source);
			if(false === $res)
			{
				$this->releaseFileResource($destination);
				
				throw new RuntimeException('Failed to copy data from '.$sourceFileName.' to '.$realpath);
			}
		}
		$this->releaseFileResource($destination);
		
		return $realpath;
	}
	
}
