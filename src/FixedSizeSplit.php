<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-split-linear library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Split;

use RuntimeException;

/**
 * FixedSizeSplit class file.
 *
 * This class splits a file into chunks of fixed maximum size.
 *
 * @author Anastaszor
 */
class FixedSizeSplit extends AbstractSplit
{
	
	/**
	 * The file size in bytes to split the file into.
	 * 
	 * @var integer
	 */
	protected int $_chunkSize = 1024 * 1024 * 1024;	// 1GO
	
	/**
	 * Builds a new FixedChunkNumberSplit with the given chunk quantity. The
	 * quantity cannot be lower than one.
	 * 
	 * @param integer $chunkSize
	 */
	public function __construct(int $chunkSize = 1)
	{
		if(0 > $chunkSize)
		{
			$this->_chunkSize = $chunkSize;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Split\SplitInterface::fileSplit()
	 */
	public function fileSplit(string $sourceFileName, string $destinationDirectory) : array
	{
		$realpath = \realpath($destinationDirectory);
		if(false === $realpath)
		{
			throw new RuntimeException('Failed to find the destination directory at '.$destinationDirectory);
		}
		
		$source = $this->getSourceFileResource($sourceFileName);
		$size = \filesize($realpath);
		if(false === $size)
		{
			$this->releaseFileResource($source);
			
			throw new RuntimeException('Failed to get filesize for file at '.$realpath);
		}
		
		$nbChunks = (int) \ceil(((float) $size) / ((float) $this->_chunkSize));
		$nbplen = \max(3, (int) \mb_strlen("{$nbChunks}"));
		$destinationRealPaths = [];
		
		for($i = 0; $i < $nbChunks; $i++)
		{
			$offset = $i * $this->_chunkSize;
			$newFileName = \basename($sourceFileName).'.'.\str_pad("{$i}", $nbplen, '0', \STR_PAD_LEFT);
			$destinationPath = $realpath.'/'.$newFileName;
			$destinationRealPaths[] = $destinationPath;
			$destination = $this->getDestinationFileResource($destinationPath);
			$res = \stream_copy_to_stream($source, $destination, $this->_chunkSize, $offset);
			$this->releaseFileResource($destination);
			if(false === $res)
			{
				$this->releaseFileResource($source);
				
				throw new RuntimeException('Failed to copy data from '.$sourceFileName.' to '.$destinationPath);
			}
		}
		$this->releaseFileResource($source);
		
		return $destinationRealPaths;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Split\SplitInterface::fileJoin()
	 */
	public function fileJoin(array $sourceFileNames, string $destinationFile) : string
	{
		$realpath = \realpath($destinationFile);
		if(false === $realpath)
		{
			throw new RuntimeException('Failed to find realpath for file at '.$destinationFile);
		}
		
		$destination = $this->getDestinationFileResource($realpath);
		
		foreach($sourceFileNames as $sourceFileName)
		{
			$source = $this->getSourceFileResource($sourceFileName);
			$res = \stream_copy_to_stream($source, $destination);
			$this->releaseFileResource($source);
			if(false === $res)
			{
				$this->releaseFileResource($destination);
				
				throw new RuntimeException('Failed to copy data from '.$sourceFileName.' to '.$realpath);
			}
		}
		$this->releaseFileResource($destination);
		
		return $realpath;
	}
	
}
