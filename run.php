<?php declare(strict_types=1);

use PhpExtended\Split\FixedChunkNumberSplit;
use PhpExtended\Split\FixedSizeSplit;

/**
 * This script is to test the split methods. (Needs require from composer).
 * 
 * Usage : 
 * php test.php chunknb <chunknb> <path_file_source> <path_dir_destination>
 * php test.php unchunk <path_destination> <path_source1> <...>
 * php test.php size <size_bytes> <path_file_source> <path_dir_destination>
 * php test.php unsize <path_destination> <path_source1> <...>
 * 
 * @author Anastaszor
 */

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the methods to use.');
}

$autoload = __DIR__.'/vendor/autoload.php';
if(!is_file($autoload))
{
	throw new RuntimeException('Composer must be runned first.');
}

require $autoload;

switch($argv[1])
{
	case 'chunknb':
		if(!isset($argv[2]))
		{
			throw new InvalidArgumentException('The second argument should be the number of chunks.');
		}
		
		if(!isset($argv[3]))
		{
			throw new InvalidArgumentException('The third argument should be the path source file.');
		}
		
		if(!isset($argv[4]))
		{
			throw new InvalidArgumentException('The fourth argument should be the destination directory.');
		}
		
		$split = new FixedChunkNumberSplit($argv[2]);
		return $split->fileSplit($argv[3], $argv[4]);
	
	case 'unchunk':
		if(!isset($argv[2]))
		{
			throw new InvalidArgumentException('The second argument should be the destination path.');
		}
		$filePaths = array_splice($argv, 3);
		$split = new FixedChunkNumberSplit();
		return $split->fileJoin($filePaths, $argv[2]);
	
	case 'size':
		if(!isset($argv[2]))
		{
			throw new InvalidArgumentException('The second argument should be the number of bytes by chunk.');
		}
		
		if(!isset($argv[3]))
		{
			throw new InvalidArgumentException('The third argument should be the path source file.');
		}
		
		if(!isset($argv[4]))
		{
			throw new InvalidArgumentException('The fourth argument should be the destination directory');
		}
		
		$split = new FixedSizeSplit($argv[2]);
		return $split->fileSplit($argv[3], $argv[4]);
	
	case 'unsize':
		if(!isset($argv[2]))
		{
			throw new InvalidArgumentException('The second argument should be the destination path.');
		}
		
		$filePaths = array_splice($argv, 3);
		$split = new FixedSizeSplit();
		return $split->fileJoin($filePaths, $argv[2]);
	
	default:
		throw new InvalidArgumentException('Unknown action, must be one of : "chunknb", "unchunk", "size", "unsize"');
}
